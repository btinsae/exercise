package com.birhane.exercise.phone;


import com.birhane.exercise.utils.PaginatedResultsRetrievedEvent;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value = "phones")
@RequiredArgsConstructor
@Tag(name = "Phone", description = "Phone API")
public class PhoneController implements PhoneApi {

    private final PhoneService phoneService;
    private final ApplicationEventPublisher eventPublisher;
    private final PhoneMapper phoneMapper;

    @Override
    public PhoneDto createPhone(PhoneDto phone) {
        return phoneMapper.phoneToPhoneDto(phoneService.createPhone(phoneMapper.phoneDtoToPhone(phone)));
    }

    @Override
    public List<PhoneDto> createPhones(List<PhoneDto> phones) {
        return phoneMapper.phoneDtosToPhones(phoneService.createPhones(phoneMapper.phonesToPhoneDtos(phones)));

    }

    @Override
    public PhoneDto getPhone(long phoneId) {
        return phoneMapper.phoneToPhoneDto(phoneService.getPhone(phoneId));

    }

    @Override
    public void deletePhone(long phoneId) {
        phoneService.deletePhone(phoneId);
    }

    @Override
    public PhoneDto updatePhone(long phoneId, PhoneDto phone) {
        return phoneMapper.phoneToPhoneDto(phoneService.updatePhone(phoneId, phoneMapper.phoneDtoToPhone(phone)));
    }

    @Override
    public ResponseEntity<PagedModel<PhoneDto>> getPhones(Pageable pageable, PagedResourcesAssembler assembler, UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        eventPublisher.publishEvent(new PaginatedResultsRetrievedEvent<>(
                PhoneDto.class, uriBuilder, response, pageable.getPageNumber(), phoneService.getPhones(pageable).getTotalPages(), pageable.getPageSize()));
        return new ResponseEntity<PagedModel<PhoneDto>>(assembler.toModel(phoneService.getPhones(pageable).map(phoneMapper::phoneToPhoneDto)), HttpStatus.OK);

    }

}
