package com.birhane.exercise.phone;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Formula;
import org.springframework.data.jpa.domain.AbstractAuditable;

import javax.persistence.*;

@Entity
@Table(name = "phones")
@Getter
@Setter
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, unique = true, length = 100)
    private String name;
    @Column(name = "technology", nullable = false, unique = true, length = 1000)
    private String technology;
    @Column(name = "two_g_band", nullable = false, unique = true, length = 1000)
    private String twoGBand;
    @Column(name = "three_g_band", nullable = false, unique = true, length = 1000)
    private String threeGBand;
    @Column(name = "four_g_band", nullable = false, unique = true, length = 1000)
    private String fourGBand;

    @Column(name = "available")
    @Formula("(select case when count(*)>0 then true else false end from phone_details pd where pd.available=true and pd.phone_id=id)")
    private Boolean available;

}