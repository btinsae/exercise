package com.birhane.exercise.phone;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PhoneMapper {
    Phone phoneDtoToPhone(PhoneDto phoneDto1);

    PhoneDto phoneToPhoneDto(Phone phone);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Phone updatePhoneFromPhoneDto(PhoneDto phoneDto1, @MappingTarget Phone phone);

    List<PhoneDto> phoneDtosToPhones(List<Phone> phones);

    List<Phone> phonesToPhoneDtos(List<PhoneDto> phones);
}
