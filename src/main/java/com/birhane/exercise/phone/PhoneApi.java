package com.birhane.exercise.phone;

import com.birhane.exercise.exception.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

public interface PhoneApi {
    @Operation(summary = "Create Phone")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Create Phone",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PhoneDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid Phone",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized access",
                    content = @Content)
    })
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    PhoneDto createPhone(@RequestBody PhoneDto phone);

    @Operation(summary = "Create multiple Phones")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Create multiple Phone",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = PhoneDto.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid Phone",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized access",
                    content = @Content)
    })

    @PostMapping("/list")
    @ResponseStatus(HttpStatus.CREATED)
    List<PhoneDto> createPhones(@RequestBody List<PhoneDto> phones);

    @Operation(summary = "Get a Phone by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the phone",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PhoneDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "phone not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized access",
                    content = @Content)})

    @GetMapping("/{phoneId}")
    @ResponseStatus(HttpStatus.OK)
    PhoneDto getPhone(@PathVariable long phoneId);

    @Operation(summary = "Delete Phone by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Phone successfully deleted",
                    content = {@Content}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "phone not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized access",
                    content = @Content)
    })

    @DeleteMapping("/{phoneId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deletePhone(@PathVariable long phoneId);

    @Operation(summary = "Update Phone ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Phone successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PhoneDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Phone not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized access",
                    content = @Content)
    })

    @PutMapping("/{phoneId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    PhoneDto updatePhone(@PathVariable long phoneId, @RequestBody PhoneDto phone);

    @Operation(summary = "Get a Phones ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get paginated Phones",
                    content = {@Content(mediaType = "application/json",
                           array = @ArraySchema(schema = @Schema(implementation = PhoneDto.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid parameter supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Phone not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized access",
                    content = @Content)
    })

    @GetMapping()
    ResponseEntity<PagedModel<PhoneDto>> getPhones(@Parameter(description = "pagination object",
            schema = @Schema(implementation = Pageable.class))
                                                   @ParameterObject @Valid Pageable pageable
            , PagedResourcesAssembler assembler
            , UriComponentsBuilder uriBuilder

            , final HttpServletResponse response);
}
