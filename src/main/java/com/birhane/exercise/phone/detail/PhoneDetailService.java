package com.birhane.exercise.phone.detail;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PhoneDetailService {
    PhoneDetail createPhoneDetail(PhoneDetail phoneDetail);

    PhoneDetail getPhoneDetail(long phoneDetailId);

    PhoneDetail getAvailablePhone(long phoneId);

    void deletePhoneDetail(long phoneDetailId);

    PhoneDetail updatePhoneDetail(long phoneDetailId, PhoneDetail phoneDetail);

    Page<PhoneDetail> getPhoneDetails(Pageable pageable);

    List<PhoneDetail> createPhoneDetails(List<PhoneDetail> phoneDetails);
}
