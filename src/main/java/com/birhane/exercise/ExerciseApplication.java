package com.birhane.exercise;

import antlr.BaseAST;
import com.birhane.exercise.security.user.User;
import com.birhane.exercise.security.user.UserRepository;
import com.birhane.exercise.utils.ApplicationProps;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

@SpringBootApplication
@EnableConfigurationProperties(ApplicationProps.class)
@RequiredArgsConstructor
public class ExerciseApplication {

	private final UserRepository repository;
	private final PasswordEncoder encoder;

	public static void main(String[] args) {
		SpringApplication.run(ExerciseApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void onAppStartUp(){
		Optional<User> admin = repository.findByUsername("Admin");
		if (admin.isEmpty()) {
			var user = new User();
			user.setUsername("Admin");
			user.setPassword(encoder.encode("6DKgfJLT56de6jFp_ "));
			user.setEnabled(true);
			user.setAccountNonExpired(true);
			user.setAccountNonLocked(true);
			user.setActive(true);
			user.setCredentialsNonExpired(true);

			repository.save(user);
		}
	}
}
