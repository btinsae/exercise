# Exercise



## Getting started

To make it easy for you to get up and running with this project, here's a list of recommended next steps.

```
cd {project_directory}
./mvnw clean package/install -DskipTests
docker compose up
```
## Introduction

This project is done to fulfill the requirement specified.To get swagger documentation head over to
[Swagger Documentation](http://localhost:8080/swagger-ui/index.html). In Addition to, the swagger documentation, I've also added postman collection file, which you may find it in the root directory of the project.

## Technology stacks
- Spring boot
- Spring data jpa
- Spring security
- Mysql
- Swagger
- JWT authentication
- Docker compose
## Some rationals
I've tried to access fonoapi as per the instruction. Unfortunately, It was not working at the time of this writing.
As a workaround, I got the data from [GSMARENA](http://GSMARENA.com).

You are not expected to populate the database by hand at least for the one's specified on the requirement document. The data.sql file under resources directory will populate the database once the application stars up.

## Why I need to add spring security?

whenever someone booked a phone, we should know who that person is. For that purpose we need to have some sort of authorization and authentication mechanise in place.
Being stateless is one of the advantages of Restful services.And, Spring security has well documented and mature support for stateless authentication and authorization.


