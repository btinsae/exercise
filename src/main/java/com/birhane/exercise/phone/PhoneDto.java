package com.birhane.exercise.phone;

import java.io.Serializable;

/**
 * A DTO for the {@link Phone} entity
 */
public record PhoneDto(Long id, String name, String technology, String twoGBand, String threeGBand,
                       String fourGBand,boolean available) implements Serializable {
}