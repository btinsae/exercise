package com.birhane.exercise.security.user;

import lombok.Data;

@Data
public class UserPasswordDto {
    private String username;
    private String oldPassword;
    private String newPassword;
}
