package com.birhane.exercise.phone.detail;

import com.birhane.exercise.phone.PhoneDto;
import lombok.Data;

import java.io.Serializable;

/**
 * A DTO for the {@link PhoneDetail} entity
 */
public record PhoneDetailDto(Integer id, PhoneDto phone, String serialNo) implements Serializable {
}