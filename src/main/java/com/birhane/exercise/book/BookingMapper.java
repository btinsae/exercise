package com.birhane.exercise.book;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookingMapper {
    Booking bookDtoToBook(BookingDto bookingDto);

   BookingDto bookingToBookingDto(Booking booking);
   BookingResponseDto bookingToBookingResponseDto(Booking booking);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Booking updateBookFromBookDto1(BookingDto bookingDto, @MappingTarget Booking booking);

    List<BookingDto> booksToBookDtos(List<Booking> bookings);

    List<Booking> bookDtosToBooks(List<BookingDto> books);
}
