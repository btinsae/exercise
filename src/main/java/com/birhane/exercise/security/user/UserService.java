package com.birhane.exercise.security.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;

public interface UserService {
    User createUser(@RequestBody() User user);

    User updateUser(long userId, @RequestBody() User user);
    User getUserByUsername(String username);

    User getUser(long userId);

    void deleteUser(long userId);

    User resetPassword(long userId, @RequestBody() UserPasswordDto passwordReset);


    Page<User> getUsers(Pageable pageable);
}
