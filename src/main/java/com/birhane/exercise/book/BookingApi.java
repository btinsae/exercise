package com.birhane.exercise.book;

import com.birhane.exercise.exception.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

public interface BookingApi {
    @Operation(summary = "Create booking")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Create booking",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookingResponseDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid booking",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
    })
    @PostMapping("/{phoneId}")
    @ResponseStatus(HttpStatus.CREATED)
    BookingResponseDto createBooking(@PathVariable Long phoneId, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestBody BookingDto book, Principal principal);

    @Operation(summary = "Create multiple bookings")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Create multiple booking",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = BookingDto.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid booking",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
    })

    @PostMapping("/list")
    @ResponseStatus(HttpStatus.CREATED)
    List<BookingDto> createBookings(@RequestBody List<BookingDto> books);

    @Operation(summary = "Get a booking by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the book",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Booking.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))})
    @GetMapping("/{bookId}")
    @ResponseStatus(HttpStatus.OK)
    BookingResponseDto getBooking(@PathVariable long bookId);

    @Operation(summary = "Delete booking by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Booking successfully deleted",
                    content = {@Content}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))})

    @DeleteMapping("/{bookId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteBooking(@PathVariable long bookId);
    @Operation(summary = "Return booked item ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Booking successfully updated",
                    content = {@Content}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Booking not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))})

    @PutMapping("/{bookingId}/return-item")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void returnBookedItem(@PathVariable long bookingId);

    @Operation(summary = "Update booking ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Booking successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Booking.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Booking not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))})

    @PutMapping("/{bookId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    BookingResponseDto updateBooking(@PathVariable long bookId, @RequestBody BookingDto book);

    @Operation(summary = "Get a bookings ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get paginated bookings",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Booking.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid parameter supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Booking not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))})
    @GetMapping()
    ResponseEntity<PagedModel<BookingDto>> getBookings(@Parameter(description = "pagination object",
            schema = @Schema(implementation = Pageable.class))
                                                       @ParameterObject @Valid Pageable pageable
            , PagedResourcesAssembler assembler
            , UriComponentsBuilder uriBuilder

            , final HttpServletResponse response);
}
