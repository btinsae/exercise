package com.birhane.exercise.phone.detail;

import com.birhane.exercise.phone.Phone;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "phone_details")
@Getter
@Setter
public class PhoneDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(cascade = CascadeType.REMOVE, optional = false)
    @JoinColumn(name = "phone_id", nullable = false)
    private Phone phone;

    @Column(name = "serial_no", nullable = false, unique = true, length = 100)
    private String serialNo;

    @Column(name = "available")
    private Boolean available;

}