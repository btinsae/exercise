insert into phones(id,name,technology,two_g_band,three_g_band,four_g_band) values
(1,'Samsung Galaxy S9','GSM / CDMA / HSPA / EVDO / LTE','GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2 (dual-SIM model only)
 	CDMA 800 / 1900 - USA','HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100 - Global, USA
 	CDMA2000 1xEV-DO - USA','1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 18, 19, 20, 25, 26, 28, 32, 38, 39, 40, 41, 66 - Global
 	1, 2, 3, 4, 5, 7, 8, 12, 13, 14, 17, 18, 19, 20, 25, 26, 28, 29, 30, 38, 39, 40, 41, 46, 66, 71 - USA'),
(2,'Samsung Galaxy S8','GSM / HSPA / LTE','GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2 (dual-SIM model only)','HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100','1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 18, 19, 20, 25, 26, 28, 32, 66, 38, 39, 40, 41'),
(3,'Motorola Nexus 6','GSM / CDMA / HSPA / LTE','GSM 850 / 900 / 1800 / 1900 - all models
 	CDMA 800 / 1900 - XT1103','HSDPA 800 / 850 / 900 / 1700 / 1800 / 1900 / 2100 - XT1100
 	HSDPA 850 / 900 / 1700 / 1900 / 2100 - XT1103
 	HSDPA 850 / 900 / 1900 / 2100 - Verizon','1, 3, 5, 7, 8, 9, 19, 20, 28, 41 - XT1100
 	2, 3, 4, 5, 7, 12, 13, 17, 25, 26, 29, 41 - XT1103
 	4, 13 - Verizon'),
(4,'Oneplus 9','GSM / CDMA / HSPA / LTE / 5G','GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2
 	CDMA 800 / 1900','HSDPA 800 / 850 / 900 / 1700(AWS) / 1800 / 1900 / 2100','1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 18, 19, 20, 25, 26, 28, 32, 38, 39, 40, 41, 66 - EU
 	1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 18, 19, 20, 25, 26, 28, 30, 32, 38, 39, 40, 41, 46, 48, 66, 71 - NA
 	1, 2, 3, 4, 5, 7, 8, 12, 17, 18, 19, 20, 26, 34, 38, 39, 40, 41, 46 - IN
 	1, 2, 3, 4, 5, 7, 8, 12, 17, 18, 19, 20, 26, 34, 38, 39, 40, 41 - CN'),
(5,'Apple iPhone 13','GSM / CDMA / HSPA / EVDO / LTE / 5G', 'GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2 (dual-SIM)
 	CDMA 800 / 1900','HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100
 	CDMA2000 1xEV-DO','1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 18, 19, 20, 25, 26, 28, 30, 32, 34, 38, 39, 40, 41, 42, 46, 48, 66 - A2633, A2634, A2635
 	1, 2, 3, 4, 5, 7, 8, 11, 12, 13, 14, 17, 18, 19, 20, 21, 25, 26, 28, 29, 30, 32, 34, 38, 39, 40, 41, 42, 46, 48, 66, 71 - A2482, A2631'),
(6,'Apple iPhone 12','GSM / CDMA / HSPA / EVDO / LTE / 5G', 'GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2 (dual-SIM) - for China
 	CDMA 800 / 1900','HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100
 	CDMA2000 1xEV-DO','1, 2, 3, 4, 5, 7, 8, 12, 13, 14, 17, 18, 19, 20, 25, 26, 28, 29, 30, 32, 34, 38, 39, 40, 41, 42, 46, 48, 66, 71 - A2172
 	1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 18, 19, 20, 25, 26, 28, 30, 32, 34, 38, 39, 40, 41, 42, 46, 48, 66 - A2403, A2404
 	1, 2, 3, 4, 5, 7, 8, 12, 13, 14, 17, 18, 19, 20, 21, 25, 26, 28, 29, 30, 32, 34, 38, 39, 40, 41, 42, 46, 48, 66, 71 - A2402'),
(7,'Apple iPhone 11','GSM / CDMA / HSPA / EVDO / LTE', 'GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2 (dual-SIM) - for China
 	CDMA 800 / 1900','HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100
 	CDMA2000 1xEV-DO','1, 2, 3, 4, 5, 7, 8, 11, 12, 13, 17, 18, 19, 20, 21, 25, 26, 28, 29, 30, 32, 34, 38, 39, 40, 41, 42, 46, 48, 66 - A2221
 	1, 2, 3, 4, 5, 7, 8, 12, 13, 14, 17, 18, 19, 20, 25, 26, 29, 30, 34, 38, 39, 40, 41, 42, 46, 48, 66, 71 - A2111, A2223'),
(8,'iPhone X','GSM / HSPA / LTE', 'GSM 850 / 900 / 1800 / 1900','HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100','1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 18, 19, 20, 25, 26, 28, 29, 30, 34, 38, 39, 40, 41, 66'),
(9,'Nokia 3310','GSM / HSPA / LTE', 'GSM 850 / 900 / 1800 / 1900','HSDPA 850 / 900 / 1900 / 2100','38, 39, 40, 41');

insert into phone_details(phone_id,serial_no,available)
values
(1,'11',true),
(2,'12',true),
(2,'13',true),
(3,'14',true),
(4,'15',true),
(5,'16',true),
(6,'17',true),
(7,'18',true),
(8,'19',true),
(9,'20',true);

