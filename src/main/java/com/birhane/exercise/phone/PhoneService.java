package com.birhane.exercise.phone;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PhoneService {
    Phone createPhone(Phone phone);

    Phone getPhone(long phoneId);

    void deletePhone(long phoneId);

    Phone updatePhone(long phoneId, Phone phone);

    Page<Phone> getPhones(Pageable pageable);

    List<Phone> createPhones(List<Phone> phones);
}
