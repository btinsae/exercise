package com.birhane.exercise.book;


import com.birhane.exercise.utils.PaginatedResultsRetrievedEvent;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "bookings")
@RequiredArgsConstructor
@Tag(name = "Booking", description = "Booking API")
public class BookingController implements BookingApi {

    private final BookingService bookService;
    private final ApplicationEventPublisher eventPublisher;
    private final BookingMapper bookingMapper;

    @Override
    public BookingResponseDto createBooking(Long phoneId, BookingDto booking, Principal principal) {
        booking.setBookedBy(principal.getName());
        return bookingMapper.bookingToBookingResponseDto(bookService.createBooking(phoneId, bookingMapper.bookDtoToBook(booking)));
    }

    @Override
    public List<BookingDto> createBookings(List<BookingDto> books) {
        return bookingMapper.booksToBookDtos(bookService.createBookings(bookingMapper.bookDtosToBooks(books)));
    }

    @Override
    public BookingResponseDto getBooking(long bookId) {
        return bookingMapper.bookingToBookingResponseDto(bookService.getBooking(bookId));
    }

    @Override
    public void deleteBooking(long bookId) {
        bookService.deleteBooking(bookId);
    }

    @Override
    public void returnBookedItem(long bookingId) {
        bookService.returnItem(bookingId);
    }

    @Override
    public BookingResponseDto updateBooking(long bookId, BookingDto book) {
        return bookingMapper.bookingToBookingResponseDto(bookService.updateBooking(bookId, bookingMapper.bookDtoToBook(book)));
    }

    @Override
    public ResponseEntity<PagedModel<BookingDto>> getBookings(Pageable pageable, PagedResourcesAssembler assembler, UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        eventPublisher.publishEvent(new PaginatedResultsRetrievedEvent<>(
                BookingDto.class, uriBuilder, response, pageable.getPageNumber(), bookService.getBookings(pageable).getTotalPages(), pageable.getPageSize()));
        return new ResponseEntity<PagedModel<BookingDto>>(assembler.toModel(bookService.getBookings(pageable).map(bookingMapper::bookingToBookingResponseDto)), HttpStatus.OK);

    }

}
