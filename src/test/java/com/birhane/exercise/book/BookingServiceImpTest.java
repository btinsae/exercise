package com.birhane.exercise.book;

import com.birhane.exercise.exception.EntityNotFoundException;
import com.birhane.exercise.phone.detail.PhoneDetail;
import com.birhane.exercise.phone.detail.PhoneDetailServiceImp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BookingServiceImpTest {
    @Mock
    BookingRepository bookingRepository;
    @Mock
    PhoneDetailServiceImp phoneDetailService;
    @InjectMocks
    BookingServiceImp bookingService;
    Booking booking;
    PhoneDetail phoneDetail;

    @BeforeEach
    void setUp() {
        booking = new Booking();
        booking.setBookedAt(LocalDateTime.now());
        booking.setBookedUntil(LocalDateTime.now().plusDays(5));
        booking.setBookedBy("Admin");
        phoneDetail = new PhoneDetail();
        phoneDetail.setId(1L);
        phoneDetail.setAvailable(true);
        phoneDetail.setSerialNo("11");
    }

    @Test
    void givenValidBookingShouldReturnBooking() {
        given(phoneDetailService.getAvailablePhone(1L)).willReturn(phoneDetail);
        given(bookingRepository.save(booking)).willReturn(booking);
        var b = bookingService.createBooking(1L, booking);
        assertThat(b).isNotNull();
        verify(bookingRepository).save(any(Booking.class));
    }

    @Test
    void givenInvalidPhoneDetailShouldThrowNotFoundException() {
        given(phoneDetailService.getAvailablePhone(2L)).willThrow(new EntityNotFoundException(PhoneDetail.class, "id", "2"));
        assertThrows(EntityNotFoundException.class, () -> bookingService.createBooking(2L, booking));
        verify(bookingRepository, never()).save(any(Booking.class));
    }

    @Test
    void givenInvalidBookingIdShouldThrowNotFoundException() {
        given(bookingRepository.findById(2L)).willReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> bookingService.getBooking(2L));
    }

    @Test
    void givenValidBookingIdShouldReturnBookingResource() {
        long id = 1L;
        given(bookingRepository.findById(id)).willReturn(Optional.ofNullable(booking));
        var b = bookingService.getBooking(id);
        assertThat(b).isNotNull();

    }

}