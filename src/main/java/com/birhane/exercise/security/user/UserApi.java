package com.birhane.exercise.security.user;

import com.birhane.exercise.book.Booking;
import com.birhane.exercise.exception.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

public interface UserApi {
    @Operation(summary = "Create User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Create User",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserResponseDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid User",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
    })
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    UserResponseDto createUser(@Valid() @RequestBody() UserDto user);
    @Operation(summary = "Update User ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Booking.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))})

    @PutMapping("/{userId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    UserResponseDto updateUser(@PathVariable("userId") long userId, @Valid() @RequestBody() UserDto user);
    @Operation(summary = "Get a User by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the book",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserResponseDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))})

    @GetMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    UserResponseDto getUser(@PathVariable("userId") long userId);
    @Operation(summary = "Delete User by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User successfully deleted",
                    content = {@Content}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))})

    @DeleteMapping("/{userId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void deleteUser(@PathVariable("userId") long userId);
    @Operation(summary = "Update User ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserResponseDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))})

    @PutMapping("/password-reset/{userId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    UserResponseDto resetPassword(@PathVariable("userId") long userId, @Valid() @RequestBody() UserPasswordDto passwordReset);

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<PagedModel<UserResponseDto>> getUsers(@Parameter(description = "pagination object",
            schema = @Schema(implementation = Pageable.class))
                                                               @ParameterObject @Valid Pageable pageable
            , PagedResourcesAssembler assembler
            , UriComponentsBuilder uriBuilder

            , final HttpServletResponse response
    );
}
