package com.birhane.exercise.phone.detail;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface PhoneDetailMapper {
    PhoneDetail phoneDetailDto1ToPhoneDetail(PhoneDetailDto phoneDetailDto);

    PhoneDetailDto phoneDetailToPhoneDetailDto(PhoneDetail phoneDetail);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    PhoneDetail updatePhoneDetailFromPhoneDetailDto(PhoneDetailDto phoneDetailDto1, @MappingTarget PhoneDetail phoneDetail);
}
