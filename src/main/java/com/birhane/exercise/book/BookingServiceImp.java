package com.birhane.exercise.book;

import com.birhane.exercise.exception.EntityNotFoundException;
import com.birhane.exercise.phone.detail.PhoneDetail;
import com.birhane.exercise.phone.detail.PhoneDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.birhane.exercise.utils.Utils.getNullPropertyNames;

@Service
@RequiredArgsConstructor
public class BookingServiceImp implements BookingService {

    private final BookingRepository bookingRepository;
    private final PhoneDetailService phoneDetailService;

    @Override
    public Booking createBooking(Long phoneId, Booking booking) {
        var availablePhone = phoneDetailService.getAvailablePhone(phoneId);
        booking.setPhoneDetail(availablePhone);
        availablePhone.setAvailable(false);
        phoneDetailService.updatePhoneDetail(availablePhone.getId(), availablePhone);
        return bookingRepository.save(booking);
    }

    @Override
    public Booking getBooking(long bookId) {
        return bookingRepository.findById(bookId)
                .orElseThrow(() -> new EntityNotFoundException(Booking.class, "Id", String.valueOf(bookId)));
    }

    @Override
    public void deleteBooking(long bookId) {
        returnItem(bookId);
        bookingRepository.deleteById(bookId);
    }

    @Override
    public void returnItem(long bookingId) {
        var booking = getBooking(bookingId);
        booking.setReturnDate(LocalDateTime.now());
        var phoneDetail = booking.getPhoneDetail();
        phoneDetail.setAvailable(true);
        phoneDetailService.updatePhoneDetail(phoneDetail.getId(), phoneDetail);
        bookingRepository.save(booking);
    }

    @Override
    public Booking updateBooking(long bookId, Booking booking) {
        var oldBook = getBooking(bookId);
        BeanUtils.copyProperties(booking, oldBook, getNullPropertyNames(booking));
        return bookingRepository.save(oldBook);
    }

    @Override
    public Page<Booking> getBookings(Pageable pageable) {
        return bookingRepository.findAll(pageable);
    }


    @Override
    public List<Booking> createBookings(List<Booking> bookings) {
        return (List<Booking>) bookingRepository.saveAll(bookings);
    }
}
