package com.birhane.exercise.book;

import com.birhane.exercise.phone.detail.PhoneDetail;
import com.birhane.exercise.security.JwtUtil;
import com.birhane.exercise.security.user.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Testcontainers
class BookControllerTest {
    @Container
    static MySQLContainer container = new MySQLContainer("mysql:latest");


    @DynamicPropertySource
    static void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("SPRING_DATASOURCE_URL", container::getJdbcUrl);
        registry.add("SPRING_DATASOURCE_USERNAME", container::getUsername);
        registry.add("SPRING_DATASOURCE_PASSWORD", container::getPassword);
    }
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;
    Booking booking;
    PhoneDetail phoneDetail;

    @BeforeEach
    void setUp() {
        booking = new Booking();
        booking.setBookedAt(LocalDateTime.now());
        booking.setBookedUntil(LocalDateTime.now().plusDays(5));
        booking.setBookedBy("Admin");
        phoneDetail = new PhoneDetail();
        phoneDetail.setId(1L);
        phoneDetail.setAvailable(true);
        phoneDetail.setSerialNo("11");
    }

    @Test
    public void givenValidBookingShouldRegister() throws Exception {
        String username = "Admin";
        String password = "6DKgfJLT56de6jFp_ ";
        String body = "{\"username\":\""+username+"\",\"password\":\""+password+" \"}";


        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/v1/login")
                        .content(body)
                        .contentType("application/json;charset=ISO-8859-1")
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andDo(result -> System.out.println(result.getResponse()))
                .andReturn();
        String resultString = mvcResult.getResponse().getContentAsString();
        JacksonJsonParser jsonParser = new JacksonJsonParser();
        String token = jsonParser.parseMap(resultString).get("token").toString();
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/v1/booking")
                        .header("Authorization", "Bearer " + token)
                        .content(objectMapper.writeValueAsString(booking))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(status().isCreated())
                .andReturn();

    }

    @Test
    public void unauthenticatedRequestShouldReturn401() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/v1/booking")
                        .content(objectMapper.writeValueAsString(booking))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(status().isCreated())
                .andReturn();

    }
}