package com.birhane.exercise.security.user;


import com.birhane.exercise.exception.EntityNotFoundException;
import com.birhane.exercise.exception.PasswordMisMatchException;
import com.birhane.exercise.exception.UserAlreadyExistsException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.birhane.exercise.utils.Utils.getNullPropertyNames;
import static java.util.Objects.isNull;

@Service
@Log4j2
@RequiredArgsConstructor
public class UserServiceImp implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public User createUser(User user) {
        if (usernameExist(user.getUsername())) {
            throw new UserAlreadyExistsException("There is an account with username '" + user.getUsername() + "'");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setFirstLogin(true);
//        user.setEnabled(!user.isEnabled() || user.isEnabled());
//        user.setActive(!user.isActive() || user.isActive());
//        user.setAccountNonExpired(!user.isAccountNonExpired() || user.isAccountNonExpired());
//        user.setAccountNonLocked(!user.isAccountNonLocked() || user.isAccountNonLocked());
//        user.setCredentialsNonExpired(!user.isCredentialsNonExpired() || user.isCredentialsNonExpired());


        return userRepository.save(user);
    }

    @Override
    public User updateUser(long userId, User user) {
        var u = getUser(userId);
        BeanUtils.copyProperties(user, u, getNullPropertyNames(user));
        u.setFullName(user.getFullName());

        if (!isNull(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setFirstLogin(true);
        }
        u.setEnabled(user.isEnabled());
        u.setActive(user.isActive());
        u.setAccountNonExpired(user.isAccountNonExpired());
        u.setAccountNonLocked(user.isAccountNonLocked());
        u.setCredentialsNonExpired(user.isCredentialsNonExpired());

        return userRepository.save(u);
    }

    @Override
    public User getUser(long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(User.class, "Id", String.valueOf(userId)));
    }

    @Override
    public void deleteUser(long userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public User resetPassword(long userId, UserPasswordDto passwordReset) {
        User user = getUserByUsername(passwordReset.getUsername());

        if (!isNull(user.getPassword())) {
            String hashedPassword = passwordEncoder.encode(passwordReset.getNewPassword());
            if (passwordEncoder.matches(passwordReset.getOldPassword(), user.getPassword())) {
                user.setPassword(hashedPassword);
            } else {
                throw new PasswordMisMatchException("Incorrect old password " + passwordReset.getOldPassword());
            }

        }
        user.setFirstLogin(false);
        return userRepository.save(user);
    }

    @Override
    public Page<User> getUsers(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    private boolean usernameExist(String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(User.class, "Username", username));
    }
}
