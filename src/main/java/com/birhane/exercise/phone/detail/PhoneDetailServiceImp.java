package com.birhane.exercise.phone.detail;

import com.birhane.exercise.exception.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.birhane.exercise.utils.Utils.getNullPropertyNames;

@Service
@RequiredArgsConstructor
public class PhoneDetailServiceImp implements PhoneDetailService {

    private final PhoneDetailRepository phoneDetailRepository;

    @Override
    public PhoneDetail createPhoneDetail(PhoneDetail phoneDetail) {
        return phoneDetailRepository.save(phoneDetail);
    }

    @Override
    public PhoneDetail getPhoneDetail(long phoneDetailId) {
        return phoneDetailRepository.findById(phoneDetailId)
                .orElseThrow(() -> new EntityNotFoundException(PhoneDetail.class, "Id", String.valueOf(phoneDetailId)));
    }

    @Override
    public PhoneDetail getAvailablePhone(long phoneId) {
        return phoneDetailRepository.findTopByPhone_IdAndAvailableTrue(phoneId)
                .orElseThrow(() -> new EntityNotFoundException(PhoneDetail.class, "Phone Id", String.valueOf(phoneId)));
    }

    @Override
    public void deletePhoneDetail(long phoneDetailId) {
        phoneDetailRepository.deleteById(phoneDetailId);
    }

    @Override
    public PhoneDetail updatePhoneDetail(long phoneDetailId, PhoneDetail phoneDetail) {
        var oldPhoneDetail = getPhoneDetail(phoneDetailId);
        BeanUtils.copyProperties(phoneDetail, oldPhoneDetail, getNullPropertyNames(phoneDetail));
        return phoneDetailRepository.save(oldPhoneDetail);
    }

    @Override
    public Page<PhoneDetail> getPhoneDetails(Pageable pageable) {
        return phoneDetailRepository.findAll(pageable);
    }


    @Override
    public List<PhoneDetail> createPhoneDetails(List<PhoneDetail> phoneDetails) {
        return (List<PhoneDetail>) phoneDetailRepository.saveAll(phoneDetails);
    }
}
