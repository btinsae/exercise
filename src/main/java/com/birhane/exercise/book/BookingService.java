package com.birhane.exercise.book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookingService {
    Booking createBooking(Long phoneId, Booking booking);

    Booking getBooking(long bookingId);

    void deleteBooking(long bookingId);

    void returnItem(long bookingId);

    Booking updateBooking(long bookingId, Booking booking);

    Page<Booking> getBookings(Pageable pageable);

    List<Booking> createBookings(List<Booking> bookings);
}
