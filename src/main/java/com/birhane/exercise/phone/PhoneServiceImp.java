package com.birhane.exercise.phone;

import com.birhane.exercise.exception.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.birhane.exercise.utils.Utils.getNullPropertyNames;

@Service
@RequiredArgsConstructor
public class PhoneServiceImp implements PhoneService {

    private final PhoneRepository phoneRepository;
    @Override
    public Phone createPhone(Phone phone) {
        return phoneRepository.save(phone);
    }

    @Override
    public Phone getPhone(long phoneId) {
        return phoneRepository.findById(phoneId)
                .orElseThrow(() -> new EntityNotFoundException(Phone.class, "Id", String.valueOf(phoneId)));
    }

    @Override
    public void deletePhone(long phoneId) {
        phoneRepository.deleteById(phoneId);
    }

    @Override
    public Phone updatePhone(long phoneId, Phone phone) {
        var oldPhone = getPhone(phoneId);
        BeanUtils.copyProperties(phone, oldPhone, getNullPropertyNames(phone));
        return phoneRepository.save(oldPhone);
    }

    @Override
    public Page<Phone> getPhones(Pageable pageable) {
        return phoneRepository.findAll(pageable);
    }


    @Override
    public List<Phone> createPhones(List<Phone> phones) {
        return (List<Phone>) phoneRepository.saveAll(phones);
    }
}
