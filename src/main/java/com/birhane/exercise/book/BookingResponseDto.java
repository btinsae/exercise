package com.birhane.exercise.book;

import com.birhane.exercise.phone.PhoneDto;
import com.birhane.exercise.phone.detail.PhoneDetail;
import com.birhane.exercise.phone.detail.PhoneDetailDto;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A DTO for the {@link Booking} entity
 */
@Data
public class BookingResponseDto implements Serializable {
    private Long id;
    private LocalDateTime bookedAt;
    private LocalDateTime bookedUntil;
    private String bookedBy;
    private PhoneDetailDto phoneDetail;
}