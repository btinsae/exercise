package com.birhane.exercise.phone.detail;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PhoneDetailRepository extends PagingAndSortingRepository<PhoneDetail,Long> {
    Optional<PhoneDetail> findTopByPhone_IdAndAvailableTrue(Long id);

}
