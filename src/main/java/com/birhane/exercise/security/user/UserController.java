package com.birhane.exercise.security.user;


import com.birhane.exercise.utils.PaginatedResultsRetrievedEvent;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("users")
@Log4j2
@RequiredArgsConstructor
@Tag(name = "User", description = "User API")
public class UserController implements UserApi {

    private final UserService userService;
    private final UserMapper userMapper;
    private final ApplicationEventPublisher eventPublisher;

    @Override
    public UserResponseDto createUser(UserDto user) {
        return userMapper.toUserResponseDTO(userService.createUser(userMapper.toUser(user)));
    }

    @Override
    public UserResponseDto updateUser(long userId, UserDto user) {
        return userMapper.toUserResponseDTO(userService.updateUser(userId, userMapper.toUser(user)));

    }

    @Override
    public UserResponseDto getUser(long userId) {
        return userMapper.toUserResponseDTO(userService.getUser(userId));

    }

    @Override
    public void deleteUser(long userId) {
        userService.deleteUser(userId);
    }

    @Override
    public UserResponseDto resetPassword(long userId, UserPasswordDto passwordReset) {
        return userMapper.toUserResponseDTO(userService.resetPassword(userId, passwordReset));
    }

    @Override
    public ResponseEntity<PagedModel<UserResponseDto>> getUsers(Pageable pageable, PagedResourcesAssembler assembler, UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        eventPublisher.publishEvent(new PaginatedResultsRetrievedEvent<>(
                UserResponseDto.class, uriBuilder, response, pageable.getPageNumber(), userService.getUsers(pageable).getTotalPages(), pageable.getPageSize()));
        return new ResponseEntity<PagedModel<UserResponseDto>>(assembler.toModel(userService.getUsers(pageable).map(userMapper::toUserResponseDTO)), HttpStatus.OK);

    }


}