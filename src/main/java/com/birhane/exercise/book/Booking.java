package com.birhane.exercise.book;

import com.birhane.exercise.phone.detail.PhoneDetail;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "bookings")
@Getter
@Setter
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "booked_at")
    private LocalDateTime bookedAt;


    @Column(name = "booked_until")
    private LocalDateTime bookedUntil;

    @Column(name = "booked_by", length = 100)
    private String bookedBy;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "phone_detail_id")
    private PhoneDetail phoneDetail;

    @Column(name = "return_date")
    private LocalDateTime returnDate;

}