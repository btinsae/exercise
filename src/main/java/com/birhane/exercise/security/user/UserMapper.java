package com.birhane.exercise.security.user;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserResponseDto toUserResponseDTO(User user);

    User toUser(UserResponseDto userResponseDTO);

    User toUser(UserDto userDTO);
}
