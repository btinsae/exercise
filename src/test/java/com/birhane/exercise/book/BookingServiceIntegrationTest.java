package com.birhane.exercise.book;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
class BookingServiceIntegrationTest {
    @Autowired
    private BookingService bookingService;
    @Container
    static MySQLContainer container = new MySQLContainer("mysql:latest");


    @DynamicPropertySource
    void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("SPRING_DATASOURCE_URL", container::getJdbcUrl);
        registry.add("SPRING_DATASOURCE_USERNAME", container::getUsername);
        registry.add("SPRING_DATASOURCE_PASSWORD", container::getPassword);
    }

    @Test
    void givenValidBookingShouldRegister() {
        Booking booking = new Booking();
        booking.setBookedAt(LocalDateTime.now());
        booking.setBookedUntil(LocalDateTime.now().plusDays(2));
        booking.setBookedBy("Birhane");
        Booking booking1 = bookingService.createBooking(1L, booking);
        assertThat(booking1).isNotNull();
    }

    @Test
    void givenUnavailablePhoneShouldReturnNotFound() {

    }

    @Test
    void returnItemShouldMakeItemAvailable() {

    }
    @Test
    void deleteBookingShouldReturnItem(){

    }
}